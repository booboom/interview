import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "@/assets/style/index.scss"
import eventBus from './plugin/eventBus1'

Vue.config.productionTip = false

Vue.use(eventBus)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
