function Person(name) {
  this.name = name
  this.orgName = 'person'
  this.arrs = []
}
Person.prototype.getName = function() {
  return this.name
}

function Child(name) {
  Person.call(this, name)
}

const child1 = new Child("child")
const child2 = new Child()
child2.arrs.push("child2")

console.log(child1, child2, child1 instanceof Person, child1.getName())

/**
 * 缺点：
 * 1. 无法识别是父级的实例
 *    child1 instanceof Person => false
 * 2. 无法继承父类原型上的属性
 *    child1.getName() => error: child1.getName is not a function
 * 3. 子类方法都在构造函数中定义，无法复用构造函数，每次创建实例都会创建一遍方法
 */

/**
 * 结论：
 *    1. 可继承父类构造函数属性
 *    2. 子类可以向父类传参
 *    3. 无法继承父类原型属性
 *    4. 不可识别为是父级的实例
 *    5. 引用类型的属性不会被其它实例共享
 */