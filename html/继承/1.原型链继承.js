function Person(name) {
  this.name = name
  this.orgName = 'person'
  this.arrs = []
}
Person.prototype.getName = function() {
  return this.orgName
}

function Child() {}
Child.prototype = new Person()

const child1 = new Child("child name")
const child2 = new Child()

child2.arrs.push("child2")


console.log(child1, child2, child1 instanceof Person, child1.getName())

/**
 * 缺点：
 * 1. 子类实例化时无法向父类传参；
 *    child1.name = undefined
 * 2. 引用类型的属性会被其它实例共享
 *    child1.arrs = ["child2"] / child2.arrs = ["child2"]
 */
/**
 * 结论：
 *    1. 可继承父类构造函数属性
 *    2. 子类无法向父类传参
 *    3. 可继承父类原型属性
 *    4. 可识别是父级的实例
 *    5. 引用类型的属性会被其它实例共享
 */