function Person(name) {
  this.name = name
  this.orgName = 'person'
  this.arrs = []
}
Person.prototype.getName = function() {
  return this.name
}
const person = new Person()

function Child(person) {
  const F = function() {}
  F.prototype = person
  return new F()
}

const child1 = new Child(person)
const child2 = new Child(person)
child2.arrs.push("child2")

console.log(child1, child2, child1 instanceof Person, child1.getName())

/**
 * 缺点：
 * 1. 所有实例都会继承原型上的属性
 * 2. 无法实现复用
 */

/**
 * 结论：
 *    1. 可继承父类构造函数属性
 *    2. 子类可以向父类传参
 *    3. 可以继承父类原型属性
 *    4. 可以识别为是父级的实例
 *    5. 引用类型的属性会被其它实例共享
 */