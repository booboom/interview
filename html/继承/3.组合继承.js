/**
 * 组合继承：结合原型链继承/构造函数继承
 * 使用原型链实现对原型方法的继承，而通过借用构造函数来实现对实例属性的继承
 * 解决了原型链继承不可向父级传参和构造函数无法继承父类原型属性的问题等，但是缺点也同样明显，会调用2次父类构造函数
 */

function Person(name) {
  this.name = name
  this.orgName = 'person'
  this.arrs = []
}
Person.prototype.getName = function() {
  return this.name
}

function Child(name) {
  Person.call(this, name)
}
Child.prototype = new Person()

const child1 = new Child("child")
const child2 = new Child()
child2.arrs.push("child2")

console.log(child1, child2, child1 instanceof Person, child1.getName())

/**
 * 缺点：
 * 1. 调用2次父类构造函数
 */

/**
 * 结论：
 *    1. 可继承父类构造函数属性
 *    2. 子类可以向父类传参
 *    3. 可以继承父类原型属性
 *    4. 可以识别为是父级的实例
 *    5. 引用类型的属性不会被其它实例共享
 */