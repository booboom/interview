/**
 * 修复了组合继承的问题
 * 
 * 寄生：在函数内返回对象然后调用
 * 组合：
 *   1. 函数的原型等于另一个实例
 *   2. 在函数中用apply或者call引入另一个构造函数，可传参
 */

function Person(name) {
  this.name = name
  this.orgName = 'person'
  this.arrs = []
}
Person.prototype.getName = function() {
  return this.name
}

function CreateInstance(person) {
  const F = function() {}
  F.prototype = person
  return new F()
}

const instance = CreateInstance(Person.prototype)

function Child(name) {
  Person.call(this, name)
}

Child.prototype = instance
instance.constructor = Child

const child1 = new Child('child1')
const child2 = new Child()

child2.arrs.push("child2")


console.log(child1, child2, child1 instanceof Person, child1.getName())

/**
 * 缺点：
 * 1. 无法实现复用
 */

/**
 * 结论：
 *    1. 可继承父类构造函数属性
 *    2. 子类可以向父类传参
 *    3. 可以继承父类原型属性
 *    4. 可以识别为是父级的实例
 *    5. 引用类型的属性不会被其它实例共享
 */