/**
 * 寄生继承就是给原型式继承外面封装一层function
 */

function Person(name) {
  this.name = name
  this.orgName = 'person'
  this.arrs = []
}
Person.prototype.getName = function() {
  return this.name
}

// 原型式继承
function CreateInstance(person) {
  const F = function() {}
  F.prototype = person
  return new F()
}

// 以上是原型式继承，再给原型式继承函数包装一层进行参数传递
function Child(obj) {
  const sub = CreateInstance(obj)
  console.log(sub)
  return sub
}

const person1 = new Person("person1")
const person2 = new Person("person2")

const child1 = new Child(person1)
const child2 = new Child(person2)

child2.arrs.push("child2")


console.log(child1, child2, child1 instanceof Person, child1.getName())

/**
 * 缺点：
 * 1. 无法实现复用
 */

/**
 * 结论：
 *    1. 可继承父类构造函数属性
 *    2. 子类可以向父类传参
 *    3. 可以继承父类原型属性
 *    4. 可以识别为是父级的实例
 *    5. 引用类型的属性不会被其它实例共享
 */