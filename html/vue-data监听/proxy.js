const data = {
  name: 'proxy'
}

// Reflect: 代替Object上的工具函数; Object.getOwnPropertyNames(target) = Reflect.ownKeys(target)

const proxyData = new Proxy(data, {
  get(target, key, receiver) {
    // 只处理本身（非原型的）属性
    const ownKeys = Reflect.ownKeys(target)
    if (ownKeys.includes(key)) {
      console.log('get => ', key)
    }
    const result = Reflect.get(target, key, receiver)
    return result
  },

  set(target, key, val, receiver) {
    // 不处理重复的数据
    if (val === target[key]) {
      return true
    }
    const result = Reflect.set(target, key, val, receiver)
    console.log('set => ', key, ':', val)
    return result
  },

  deleteProperty(target, key) {
    const result = Reflect.deleteProperty(target, key)
    console.log('delete => ', key)
    return result
  }
})